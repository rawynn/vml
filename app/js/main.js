$(function () {

    if ((window.location.href === window.location.origin + "/VML/top-players.html")) {
        if ($(window).width() < 1024) {
            $('.boxesWrapper').slick();
        } else {
            if ($('.boxesWrapper').hasClass('slick-slide')) {
                $('.boxesWrapper').slick('unslick');
            }
        }

        $(window).resize(function () {
            if (($(window).width() >= 1010) && ($(window).width() <= 1100)) {
                location.reload();
            }
            if ($(window).width() < 1024) {
                $('.boxesWrapper').slick();
            } else {
                if ($('.boxesWrapper').hasClass('slick-slide')) {
                    $('.boxesWrapper').slick('unslick');
                }
            }
        });
    }

    const flatDefault = [
        [1, 2, [3]], 4
    ];


    //Approach 1: not compatible with all browsers
    function flatten(flatIt) {
        if (flatIt === '') {
            flatIt = flatDefault;
        }
        let parsed = JSON.parse("[" + flatIt + "]");
        let flattened = parsed.flat(2);
        $('.result').html('[' + flattened + ']');
    }
    //not compatible with all browsers

    //Approach 2
    function flattenAlternative(flatIt) {
        return flatIt.reduce((acc, val) => Array.isArray(val) ? acc.concat(flattenAlternative(val)) : acc.concat(val), []);
    }

    $('.flatButton').on("click", function () {
        // flatten($('input.flatInput').val());  //Approach 1: not compatible with all browsers

        //Approach 2
        let flatIt;
        if ($('input.flatInput').val() === '') {
            flatIt = flatDefault;
        } else {
            flatIt = $('input.flatInput').val();
        }
        let parsed = JSON.parse("[" + flatIt + "]");
        let flattened = flattenAlternative(parsed);

        //test if array is flattened
        for (let i = 0; i < flattened.length; i++) {
            if (Array.isArray(flattened[i]) === true) {
                alert("Array is not correctly flattened! ");
                break;
            }
        }

        $('.result').html('[' + flattened + ']');
    });


    var clickDisabled = false;

    function disable(time) {
        clickDisabled = true;
        setTimeout(function () {
            clickDisabled = false;
        }, time);
    }


    $('.hamburger').on("click", function () {
        if ($(this).hasClass('menuOpened')) {
            if (clickDisabled === false) {
                $(this).removeClass('menuOpened');
                $('.menu li').fadeOut("fast");
                disable(1000);
            }
        } else {
            if (clickDisabled === false) {
                $(this).addClass('menuOpened');
                // $('.menu li').show(500);
                setTimeout(function () {
                    $(".menu li").first().fadeIn("fast", function showNext() {
                        $(this).next(".menu li").fadeIn("fast", showNext);
                    });
                }, 300);
                disable(1000);
            }
        }

    });
});